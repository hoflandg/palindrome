package Sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIsPalindrome() {
		assertTrue("Invalid value for palindrome", Palindrome.isPalindrome("anna"));
	}
	
	@Test
	public void testIsPalindromeBoundaryIn() {
		assertTrue("Invalid value for palindrome", Palindrome.isPalindrome("a"));
	}
	
	@Test
	public void testIsPalindromeBoundaryOut() {
		assertFalse("Invalid value for palindrome", Palindrome.isPalindrome("arna"));
	}
	
	@Test
	public void testIsPalindromeException() {
		assertFalse("Invalid value for palindrome", Palindrome.isPalindrome("abc"));
	}



}
