package Sheridan;

public class Palindrome {
	
	public static Boolean isPalindrome(String input) {

		input = input.toUpperCase();
		input = input.replaceAll(" ", "");
		

		
		for(int i = 0, j = input.length() - 1; i < j; i++, j--) {
			if(input.charAt(i) != input.charAt(j)) {
				return false;
			}
		}
		
		return true;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(isPalindrome("anna"));
		


	}

}
